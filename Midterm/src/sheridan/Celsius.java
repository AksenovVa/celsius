package sheridan;

public class Celsius {
	public static int fromFahrenheit(String temperature) throws NumberFormatException
	{
		int temp = Integer.parseInt(temperature);
		return (temp - 32) * 5 / 9;
	}

}
