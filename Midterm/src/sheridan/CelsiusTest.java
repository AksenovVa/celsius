package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class CelsiusTest {

	@Test
	public void RegularTestFromFahrenheit() {
		int result = Celsius.fromFahrenheit("32");
		assertEquals(0, result);
	}
	@Test (expected=NumberFormatException.class)
	public void ExceptionalTestFromFahrenheit() {
		int result = Celsius.fromFahrenheit("abc");
		fail("type is not valid");
	}
	@Test
	public void BoundaryInTestFromFahrenheit() {
		int result = Celsius.fromFahrenheit("33");
		assertEquals(0, result);
	}
	@Test
	public void BoundaryOutTestFromFahrenheit() {
		int result = Celsius.fromFahrenheit("31");
		assertEquals(0, result);
	}

}
